new Vue({
    el: "#app",
    data: {
        playerHealth: 100,
        monsterHealth: 100,
        gameIsRunning: false,
        turns: []
    },
    methods: {
        startGame: function () {
            this.gameIsRunning = true;
            this.monsterHealth = 100;
            this.playerHealth = 100;
            this.turns = [];
        },
        attack: function () {
            var dmg = this.calculateDmg(3, 10);
            this.monsterHealth -= dmg
            this.logger(true, dmg)
            if (this.checkWin()) {
                return;
            }
            this.monsterAttacks();
        },
        specialAttack: function () {
            var dmg = this.calculateDmg(10, 20);
            this.monsterHealth -= dmg
            this.logger(true, dmg)
            if (this.checkWin()) {
                return;
            }
            this.monsterAttacks();
        },
        heal: function () {
            if (this.playerHealth <= 90) {
                this.playerHealth += 10;

            } else {
                this.playerHealth = 100;
            }
            this.turns.unshift({
                isPlayer: true,
                text: 'players Health restored to ' + this.playerHealth
            });
            this.monsterAttacks();
        },
        giveUp: function () {
            this.gameIsRunning = false;
        },
        logger: function (player, dmg) {
            if (player) {
                this.turns.unshift({
                    isPlayer: true,
                    text: 'Player dealt dmg ' + dmg
                });
            } else {
                this.turns.unshift({
                    isPlayer: false,
                    text: 'monster dealt dmg ' + dmg
                });
            }
            console.log(this.turns);
        },
        monsterAttacks: function () {
            var dmg = this.calculateDmg(5, 12);
            this.playerHealth -= dmg
            this.logger(false, dmg)
            this.checkWin();
        },
        calculateDmg: function (min, max) {
            return Math.max(Math.floor(Math.random() * max) + 1, min);
        },
        checkWin: function () {
            if (this.monsterHealth <= 0) {
                if (confirm("You won! New game")) {
                    this.startGame();
                } else {
                    this.gameIsRunning = false
                } return true;
            } else if (this.playerHealth <= 0) {
                if (confirm("You lost! New game")) {
                    this.startGame();
                } else {
                    this.gameIsRunning = false
                } return true;
            } return;
        }
    }
});
